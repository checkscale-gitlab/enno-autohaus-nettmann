<?php

function endSession()
{
    session_destroy();
    echo GoToNow("/welcome");
}

function zeigeLoginFormular($errorMessage = "")
{
    $loginFormular = "";
    $loginFormular .= '<form action="" method="post">';
    $loginFormular .= '<br>Benutzername:</br>';
    $loginFormular .= '<input type="username" size="40" maxlength="250" name="username"><br/>';
    $loginFormular .= '<br>Dein Passwort:</br>';
    $loginFormular .= '<input type="password" size="40"  maxlength="250" name="password"><br/>';
    $loginFormular .= "<br>" . $errorMessage . "</br>";
    $loginFormular .= '';
    $loginFormular .= '<button type="submit" name="btnLogin" value="True">Login</button>';
    $loginFormular .= '</form>';
    return $loginFormular;
}

function checkLoginData($username, $password)
{
    if($username == "" || $password == "") {
        return false;
    }

    $db_instance = DatabaseConnectionNettmann::getInstance();
    $user = $db_instance->getUserData($username);

    if ($user !== false && password_verify($password, $user['password'])) {
        return True;
    }
    return False;
}

function generatePassword($password){
    $options = [
        'cost' => 12,
    ];
    return password_hash($password, PASSWORD_BCRYPT, $options);
}
