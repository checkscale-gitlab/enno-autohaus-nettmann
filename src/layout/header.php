<?php

if (!@include "constants.php") {
    echo "Konstanten-Include konnte nicht gefunden werden. Bitte repariere das Applikationssetup!";
    exit(1);
}

function getHeader()
{
    $data = "";
    $data .= "<header class='flex-container'>";
    $data .= "<h1>Autohaus Nettmann</h1>";
    $data .= "<img class='pullLastFlexboxBoxRight' src='/img/logo_Nettmann.png' alt='Logo Autohaus Nettmann'>";
    $data .= "</header>";
    return $data;
}

function getNavbar($group, $username)
{
    $data = "";
    $data .= "<div class='navbar flex-container'>";
    $data .= "<a href='/'>Startseite</a>";
    if ($group == GROUP_CUSTOMER) {
        $data .= "<a href='/booking'>Auto buchen</a>";
    } elseif ($group == GROUP_EMPLOYEE) {
        $data .= "<a href='/building%20access'>Zutrittsversuche</a>";
        $data .= "<a href='/calendar'>Kalendar</a>";
    }
    if ($username == "") {
        $data .= "<a href='/login' class='pullLastFlexboxBoxRight'>Login</a>";
    } else {
        $data .= "<a href='/logout' class='pullLastFlexboxBoxRight'>Logout</a>";
    }
    $data .= "</div>";
    return $data;
}
