<?php

function showRentalCarForm()
{
    $db_instance = DatabaseConnectionNettmann::getInstance();
    $car_classes = $db_instance->getCarClasses();
    $cars = $db_instance->getCars();
    $supplementary_equipments = $db_instance->getSupplementaryEquipment();
    $carparks = $db_instance->getCarParks();

    $data = "";
    $data .= "<form id='regForm' action='/showReciept' method='post'>";
    $data .= "<label><h2>Auto mieten</h2></label>";
    $data .= '<p>Kundennummer: ' . $_SESSION["userID"] . "</p>";

    // Car Class
    $data .= "<div class='tab'>Welche Fahrzeugklasse bevorzugen Sie:";
    $data .= "<fieldset>";
    for ($i = 1; $i < sizeof($car_classes) + 1; $i++) {
        $car_class = $car_classes[$i];
        $data .= '<div class="oneline">';
        $data .= "<input type='radio' name='car_class' id='radio-$car_class' value='$i'>";
        $data .= "<label for='radio-$car_class'>$car_class</label>";
        $data .= "</div>";
    }
    $data .= "</fieldset>";
    $data .= "</div>";

    // Car Model (in Class)
    $data .= "<div class='tab'>Welches Auto bevorzugen Sie: ";
    $data .= "<fieldset>";
    $data .= "<select name='car_model'>";
    foreach ($cars as $car) {
        $data .= "<option data-carclass='$car->car_class_ID' value='$car->ID'>" . $car->manufacturer . ' - ' . $car->model . ' (' . $car->pricePerDay . ' EUR Netto)</option>';
    }
    $data .= "</select>";
    $data .= "</fieldset>";
    $data .= "</div>";

    // Additional Equipment (specific to a car)
    $data .= "<div class='tab'>Welche Zusatzausstattung bevorzugen Sie:";
    $data .= "<fieldset id='checkbox-supplementaryequipment'>";
    foreach ($supplementary_equipments as $supplementary_equipment) {
        $data .= '<div class="oneline">';
        $data .= "<input type='checkbox' value='$supplementary_equipment->ID' name='supplementary_equipment[]' id='checkbox-$supplementary_equipment->ID' onclick='check_checkboxes()'/>";
        $data .= "<label for='checkbox-$supplementary_equipment->ID'>$supplementary_equipment->name</label>";
        $data .= '</div>';
    }
    $data .= "</fieldset>";
    $data .= "</div>";

    // Rental Station (the car will be there just magically to not complicate things).
    $data .= '<div class="tab">Wo wollen Sie das Auto abholen:';
    $data .= "<fieldset>";
    foreach ($carparks as $carpark) {
        $data .= '<div class="oneline">';
        $data .= "<input type='radio' name='carpark' id='radio-$carpark->ID' value='$carpark->ID'>";
        $data .= "<label for='radio-$carpark->id'>$carpark->name</label>";
        $data .= '</div>';
    }
    $data .= "</fieldset>";
    $data .= "</div>";

    // Buttons and Step bubbles
    $data .= '<div style="overflow:auto;">';
    $data .= '<div style="float:right;">';
    $data .= '<button type="reset" id="clearBtn" onclick="resetForm()">Reset Form</button>';
    $data .= '<button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>';
    $data .= '<button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>';
    $data .= '</div>';
    $data .= '</div>';
    $data .= '<div style="text-align:center;margin-top:40px;">';
    $data .= '<span class="step"></span>';
    $data .= '<span class="step"></span>';
    $data .= '<span class="step"></span>';
    $data .= '<span class="step"></span>';
    $data .= '</div>';
    $data .= '</form>';
    return $data;
}

function showReciept()
{
    if (isset($_POST["carpark"])
        && isset($_POST["supplementary_equipment"])
        && isset($_POST["car_model"])
        && isset($_POST["car_class"])) {
        $db_instance = DatabaseConnectionNettmann::getInstance();
        $carclass = null;
        $car = null;
        $supplementary_equipment_objects = array();
        $supplementary_equipment_text = "";
        $carpark = null;

        if (is_numeric($_POST["car_model"])) {
            $car = $db_instance->getCarById((int)$_POST["car_model"]);
        }

        if (is_numeric($_POST["carpark"])) {
            $carpark = $db_instance->getCarParkById((int)$_POST["carpark"]);
        }

        if (is_numeric($_POST["car_class"])) {
            $carclass = $db_instance->getCarClassById((int)$_POST["car_class"]);
        }

        if (is_null($car) || is_null($carpark) || is_null($carclass)) {
            return GoToNow("/welcome");
        }

        foreach ($_POST["supplementary_equipment"] as $key => $value) {
            if (is_int($key)) {
                $db_result = $db_instance->getSupplementaryEquipmentOfCar((int)$_POST["car_model"]);
                if ($db_result == null) {
                    return GoToNow("/welcome");
                }
                $supplementary_equipment_objects[$key] = $db_result[$key];;
            } else {
                return GoToNow("/welcome");
            }
        }

        foreach ($supplementary_equipment_objects as $key => $value) {
            $supplementary_equipment_text .= $supplementary_equipment_objects[$key]->name . '; ';
        }
        $supplementary_equipment_text = substr($supplementary_equipment_text, 0, -2);

        $data = "<div>";
        $data .= '<table>';
        $data .= '<tr>';
        $data .= '<td>Klasse</td>';
        $data .= "<td>$carclass->name</td>";
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td>Hersteller</td>';
        $data .= "<td>$car->manufacturer</td>";
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td>Modell</td>';
        $data .= "<td>$car->model</td>";
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td>Zusatzaustattung</td>';
        $data .= "<td>$supplementary_equipment_text</td>";
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td>Preis pro Tag (netto)</td>';
        $data .= "<td>$car->pricePerDay &euro;</td>";
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td>Preis pro Tag (brutto)</td>';
	$price = $car->pricePerDay + $car->pricePerDay * 0.19;
	$price = round($price, 2);
        $data .= "<td>$price &euro;</td>";
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td>Abholort</td>';
        $data .= "<td>$carpark->name (Lat: $carpark->latitude, Long: $carpark->longitude)</td>";
        $data .= '</tr>';
        $data .= '</table>';
        $data .= '</div>';
    } else {
        $data = GoToNow("/welcome");
    }
    return $data;
}
